var board = $('.js-board');
var letterExample = $('.js-letter');
var wrongGuess = 0;
var maxWrongGuess = 4;

function drawLetter(letter) {
    var newLetter = letterExample.clone();
    newLetter.text(letter);
    if (letter == ' ') {
        newLetter.addClass('space');
    }
    board.append(newLetter);
}

function createSolution(solution) {
    board.empty();
    var splittedSolution = solution.split('');
    for (var i = 0; i < splittedSolution.length; i++) {
        drawLetter(currentSolution[i]);
    }
}

function addMessage(msg) {
    $('.js-status').prepend($('<br>'));
    $('.js-status').prepend(msg);
}

function pickSolution(data) {
    var dataCount = data.length;
    var randomIndex = Math.floor(dataCount * Math.random());
    return data[randomIndex].toLowerCase();
}


function checkLetter(letter) {
    var letters = $('.board').children('.js-letter');

    var guessed = 0;
    var letterExists = false;

    letters.each(function () {

        if ($(this).text() === letter) {
            if ($(this).hasClass('folded')) {
                $(this).removeClass('folded');
                guessed++;
            }
            letterExists = true;
        }
    });


    if (letterExists === false) {
        wrongGuess++;
        var remainingGuesses = maxWrongGuess - wrongGuess;
        addMessage('Litera ' + letter + ' nie jest poprawna, masz jeszcze ' + remainingGuesses + ' mozliwosci');
        if (wrongGuess >= maxWrongGuess) {
            addMessage('przegrałeś');
        }
    } else if (letterExists === true && guessed === 0) {
        addMessage('litera ' + letter + ' byla juz zgadnieta');
    } else {
        addMessage('Zgadles litere: ' + letter + '. Odslonieto ' + guessed + ' liter');
        checkSuccess();
    }

}

function checkSuccess() {
    if ($(".folded").length === countSpaces) {
        addMessage("Wygrales!")
    }
}

var solutions = ["Raz kozie śmierć", "Dla chcącego nic trudnego"];

var currentSolution = pickSolution(solutions);
createSolution(currentSolution);
var countSpaces = currentSolution.split(' ').length - 1;
$('.js-input').on('input', function () {
    var guessLetter = $(this).val().toLowerCase();
    $(this).val('');
    checkLetter(guessLetter);

});


$('.js-solution').on('change', function () {

    var guessedSolution = $(this).val().toLowerCase();
    if (guessedSolution === currentSolution) {
        addMessage('Wygrałeś');
        $('.js-board').children('.js-letter:not(.space)').removeClass('folded');
    } else {
        addMessage('Przegrałeś')
    }

});
